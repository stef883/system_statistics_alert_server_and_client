import configparser
import psycopg2 # library to support connections with postgre databases

# init configparser
config = configparser.ConfigParser()
config.read('config.conf')

# database configuration
db_user = config["DATABASE"]["user"]
db_pass = config["DATABASE"]["pass"]
db_database = config["DATABASE"]["database"]
db_host = config["DATABASE"]["host"]
db_port = config["DATABASE"]["port"]

class myDBconnection:
    def get_connection(self):
        conn = psycopg2.connect(database=db_database, user=db_user, password=db_pass, host=db_host, port=db_port)
        return conn

    def insert_row(self,conn,data_dict):
        # insers a row into the "statistics" table.
        cur = conn.cursor()
        values = (data_dict["cpu_user"],data_dict["cpu_nice"],\
                                            data_dict["cpu_system"],data_dict["cpu_idle"], data_dict["cpu_iowait"],\
                                            data_dict["cpu_irq"],data_dict["cpu_softirq"],\
                                            data_dict["cpu_steal"], data_dict["cpu_guest"], data_dict["cpu_guest_nice"],\
                                            data_dict["virtual_memory_total"], data_dict["virtual_memory_available"],\
                                            data_dict["virtual_memory_percent"], data_dict["virtual_memory_free"],\
                                            data_dict["virtual_memory_active"], data_dict["virtual_memory_inactive"],\
                                            data_dict["virtual_memory_buffers"], data_dict["virtual_memory_cached"],\
                                            data_dict["virtual_memory_shared"], data_dict["swap_memory_total"],\
                                            data_dict["swap_memory_used"], data_dict["swap_memory_free"],
                                            data_dict["swap_memory_percent"],\
                                            data_dict["swap_memory_sin"], data_dict["swap_memory_sout"],\
                                            data_dict["uptime"], data_dict['num_of_processes'])
        cur.execute("INSERT INTO statistics (cpu_user, cpu_nice, cpu_system, cpu_idle, cpu_iowait,\
                                             cpu_irq, cpu_softirq, cpu_steal, cpu_guest,\
                                             cpu_guest_nice, virtual_memory_total,\
                                             virtual_memory_available, virtual_memory_percent,\
                                             virtual_memory_free, virtual_memory_active,\
                                             virtual_memory_inactive, virtual_memory_buffers,\
                                             virtual_memory_cached, virtual_memory_shared,\
                                             swap_memory_total, swap_memory_used,\
                                             swap_memory_free, swap_memory_percent,\
                                             swap_memory_sin, swap_memory_sout,\
                                             uptime, num_of_processes) VALUES ({0}, {1}, {2},\
                                             {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10},\
                                             {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18},\
                                             {19}, {20}, {21}, {22}, {23}, {24}, '{25}', {26});".format(*values))
        conn.commit()
    def close_connection(self, conn):
        conn.close()
