from paramiko import SSHClient
from bs4 import BeautifulSoup as bs
import myDBconnection
from server_tools import *
from Client import Client # this is just a Client class
                          # used to store 4 fields of information

class Server:
    def __init__(self, ssh, clients_file, db):
        self.ssh = ssh
        self.clients_file = clients_file
        self.db = db
    def run(self):
        #Initialize ssh
        init_ssh(self.ssh)

        clients_doc = self.clients_file.read()
        soup = bs(clients_doc, "html.parser")
        conn = self.db.get_connection()

        # for each client instance in the xml file
        for client_instance in soup.find_all("client"):
            # init Client object
            client = Client(client_instance["ip"],\
                            client_instance["username"],\
                            client_instance["password"],\
                            client_instance["mail"])

            stats = get_stats(self.ssh,client)
            self.db.insert_row(conn,stats)
            print("data of client with ip: " + client.ip +" inserted into DB")
            for alert in client_instance.find_all("alert"):
                # check to see if any email-alert should be raised.
                # If yes, raise it
                evaluate_alert(alert,stats,client)

        # close database connection
        conn.close()

if __name__ == "__main__":
    s = Server(SSHClient(),open("clients.xml", "r"),myDBconnection.myDBconnection())
    s.run()