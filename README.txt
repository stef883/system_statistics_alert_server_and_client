[Instructions to install and configure prerequisites or dependencies, if any]

Assuming that we have python3 installed on the system, we need to add some extra libraries.
We run these commands (some might need root privileges):

    pip3 install psutil
    pip3 install pycrypto
    pip3 install paramiko
    pip3 install scp
    pip3 install beautifulsoup4
    pip3 install psycopg2

[Instructions to create and initialize the database (if required)]
From the "sql scripts" directory we need to run the scripts:

    create_database.sql
    create_user.sql
    grant_permission.sql
    create_table_statistics.sql

[Issues faced]

    There were some trouble taking the output of the command that run on the clients. I could not
    use the stdout, for encryption/decryption reasons. I overcame this obstacle by outputing
    the ouput of the command to a file and then getting the file via a secure channel with scp.
