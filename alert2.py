class Alert:
    def __init__(self, type_alert, limit, value):
        self.type_alert = type_alert
        self.limit = limit
        self.value = value

    def __str__(self):
        return  "type: " + str(self.type_alert) +\
                "\nlimit: " + str(self.limit) +\
                "\nValue" + str(self.value) + "\n"