import myDBconnection
from server_tools import *
from Client import Client

def test_simulate_client():
    # make a test client
    client1 = Client("192.168.0.1","stefanos","1234","stefanos6226@gmail.com")
    client1.stats = {}
    client1.stats["cpu_guest"] = 0.0
    client1.stats["cpu_iowait"] = 1599.06
    client1.stats["swap_memory_sout"] = 0
    client1.stats["virtual_memory_inactive"] = 4861489152
    client1.stats["cpu_nice"] = 0.0
    client1.stats["swap_memory_total"] = 0.0
    client1.stats["cpu_softirq"] = 0.0
    client1.stats["virtual_memory_available"] = 0.0
    client1.stats["cpu_user"] = 0.0
    client1.stats["cpu_idle"] = 0.0
    client1.stats["cpu_guest_nice"] = 0.0
    client1.stats["num_of_processes"] = 201
    client1.stats["virtual_memory_cached"] = 0.0
    client1.stats["virtual_memory_active"] = 0.0
    client1.stats["cpu_irq"] =- 0.0
    client1.stats["uptime"] = "6:00:16.803934"
    client1.stats["swap_memory_percent"] = 0.0
    client1.stats["virtual_memory_free"] = 0.0
    client1.stats["virtual_memory_buffers"] = 0.0
    client1.stats["virtual_memory_total"] = 0.0
    client1.stats["virtual_memory_percent"] = 0.0
    client1.stats["swap_memory_used"] = 0.0
    client1.stats["swap_memory_sin"] = 0.0
    client1.stats["swap_memory_free"] = 0.0
    client1.stats["cpu_steal"] = 0.0
    client1.stats["virtual_memory_shared"] = 0.0
    client1.stats["cpu_system"] = 0.0

    alert1 = {}
    alert1["typealert"] = "num_of_processes"
    alert1["limit"] = 150
    alert1["value"] = client1.stats[alert1["typealert"]]

    client1.alerts.append(alert1)
    clients = (client1,)

    myDB = myDBconnection.myDBconnection()
    conn = myDB.get_connection()

    for client in clients:
        stats = client.stats
        myDB.insert_row(conn,stats)
        # print("data inserted in db") #do not insert test data in db
        for alert in client.alerts:
            evaluate_alert(alert,stats,client)
    print("--- TEST 'SIMULATE CLIENT' COMPLETE ---")

def test_send_email():
    body = "Test body"
    subject = "Test subject"
    recipient = "stefanos6226@gmail.com"
    sender = "home_server"

    send_msg(body, recipient, sender, subject)
    print("Email sent")
    print("--- TEST 'SEND EMAIL' COMPLETE ---")
if __name__ == "__main__":
    test_simulate_client()
    test_send_email()