import json
import smtplib
from paramiko import AutoAddPolicy
from scp import SCPClient, SCPException
from Crypto.Cipher import AES
from alert2 import Alert


def get_stats(ssh, client):
    # returns json objects. I use json instead of
    # a normal python dictionary for encryption purposes.
    ssh.connect(client.ip, username=client.username, password=client.password)

    # use of scp library that works with paramiko to return a secure copy channel
    scp = SCPClient(ssh.get_transport())
    scp.put('script.py', '/tmp/script.py')
    (stdin, stdout, stderr) = ssh.exec_command("python3 /tmp/script.py")
    scp.get("/tmp/out.txt", "out.txt")# response file from the client's machine
    scp.close()
    ssh.close()

    key = '0123456789abcdef' # must be the same with the script.py's key
    cipher = AES.new(key) # i use AES
    out_file = open("out.txt","rb")

    output = out_file.read()

    decrypted = decrypt(cipher,output) # json object
    data_dict = json.loads(decrypted) # transform to python dict first
    return data_dict


def init_ssh(ssh):
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(AutoAddPolicy())

def decrypt(cipher, ciphertext):
    dec = cipher.decrypt(ciphertext).decode("utf-8")
    l = dec.count("{")
    return dec[:len(dec) - l + 1]

def evaluate_alert(alert, data_dict, client):
    # it sends email if needed
    al = Alert(alert["typealert"], alert["limit"], data_dict[alert["typealert"]])
    if al.value > float(al.limit):
        msg = send_msg(str(alert), recipient=client.mail, \
                       sender="home_server", subject="Alert on " + client.ip)
        print("Alert of type '" + al.type_alert + "' sent to " + client.mail + " about the state of " + client.ip )


def send_msg(body, recipient, sender, subject):
    to = recipient
    # instead of setting a server i used gmail's smtp with a test account
    gmail_user = 'dokimastiko954@gmail.com'
    gmail_pwd = 'n7XeZ4q3GHwnof2krfu8XYGA'
    try:
        smtpserver = smtplib.SMTP("smtp.gmail.com", 587)
        smtpserver.ehlo()
        smtpserver.starttls()
        smtpserver.ehlo
        smtpserver.login(gmail_user, gmail_pwd)
        header = 'To:' + recipient + '\n' + 'From: ' + sender + '\n' + 'Subject:' + subject +'\n'
        msg = header + '\n' + body + '\n\n'
        smtpserver.sendmail(gmail_user, to, msg)

        smtpserver.close()
    except smtplib.SMTPDataError:
        print("The SMTP server refused to accept the message data.")
    except smtplib.SMTPConnectError:
        print("Error occurred during establishment of a connection with the server.")
    except smtplib.SMTPAuthenticationError:
        print("SMTP authentication went wrong. Most probably the server didn’t\
               accept the username/password combination provided.")
