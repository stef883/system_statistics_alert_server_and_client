import json
import psutil # http://psutil.readthedocs.io/en/latest/
import datetime
from Crypto.Cipher import AES # pycrypto library

def my_print(x):
    return str(x)

def pad(s):
    # padding for the encryption
    return s + ((16 - len(s) % 16) * "{")

def encrypt(cipher, plaintext):
    # encrypting function
    return cipher.encrypt(pad(plaintext))

def collect_stats(): # returns statistics in encrypted form
    # dictionary that will hold the different statistics
    stats = {}

    cpu_times = psutil.cpu_times() # get cpu times related statistics
    virtual_memory = psutil.virtual_memory() # get virtual memory related statistics
    swap_memory = psutil.swap_memory() # get swap memory related statistics

    # Insert the statistics into the stats dictionary
    stats["cpu_user"] = cpu_times[0] # time spent by normal processes executing
                               # in user mode; on Linux this also includes guest time
    stats["cpu_nice"] = cpu_times[1] # time spent by niced (prioritized) processes executing
                               #  in user mode; on Linux this also includes guest_nice time
    stats["cpu_system"] = cpu_times[2] # time spent by processes executing in kernel mode
    stats["cpu_idle"] = cpu_times[3] # time spent doing nothing
    stats["cpu_iowait"] = cpu_times[4] # time spent waiting for I/O to complete
    stats["cpu_irq"] = cpu_times[5] # (Linux, BSD): time spent for servicing hardware interrupts
    stats["cpu_softirq"] = cpu_times[6] # (Linux): time spent for servicing software interrupts
    stats["cpu_steal"] = cpu_times[7] # time spent by other operating systems running
                                # in a virtualized environment
    stats["cpu_guest"] = cpu_times[8] # time spent running a virtual CPU for guest
                                # operating systems under the control of the Linux kernel
    stats["cpu_guest_nice"] = cpu_times[9] # time spent running a niced guest
                                     # (virtual CPU for guest operating systems under
                                     # the control of the Linux kernel)

    stats["virtual_memory_total"] = virtual_memory[0] # total physical memory.
    stats["virtual_memory_available"] = virtual_memory[1] # the memory that can be given instantly to
                                                          # processes without the system going into swap.
                                                          # This is calculated by summing different memory
                                                          # values depending on the platform and
                                                          # it is supposed to be used to monitor actual memory usage
                                                          # in a cross platform fashion.
    stats["virtual_memory_percent"] = virtual_memory[2]
    stats["virtual_memory_free"] = virtual_memory[3] # memory not being used at all (zeroed) that is readily
                                                     # available; note that this doesn’t reflect the actual memory
                                                     # available (use available instead). total -
                                                     # used does not necessarily match free.
    stats["virtual_memory_active"] = virtual_memory[4] # (UNIX): memory currently in use or very recently used,
                                                       # and so it is in RAM.
    stats["virtual_memory_inactive"] = virtual_memory[5] # (UNIX): memory that is marked as not used.
    stats["virtual_memory_buffers"] = virtual_memory[6] # (Linux, BSD): cache for things like file system metadata.
    stats["virtual_memory_cached"] = virtual_memory[7] # (Linux, BSD): cache for various things.
    stats["virtual_memory_shared"] = virtual_memory[8] # (Linux, BSD): memory that may be simultaneously
                                                       # accessed by multiple processes.
    stats["swap_memory_total"] = swap_memory[0] # total swap memory in bytes
    stats["swap_memory_used"] = swap_memory[1] # used swap memory in bytes
    stats["swap_memory_free"] = swap_memory[2] # free swap memory in bytes
    stats["swap_memory_percent"] = swap_memory[3] # the percentage usage calculated as
                                                  # (total - available) / total * 100
    stats["swap_memory_sin"] = swap_memory[4] # the number of bytes the system has swapped
                                              # in from disk (cumulative)
    stats["swap_memory_sout"] = swap_memory[5] # the number of bytes the system has swapped
                                               # out from disk (cumulative)

    stats["uptime"] = str(datetime.datetime.now() - datetime.datetime.fromtimestamp(psutil.boot_time())).replace(",", "") # the uptime of the machine in str format
    stats["num_of_processes"] = len(psutil.pids()) # number of process runnin

    # jsonify to be able to get encrypted
    stats = json.dumps(stats, ensure_ascii=False)
    return stats

key = '0123456789abcdef' # 16 bytes long. The same key can be found in the server
cipher = AES.new(key) # use an AES cipher
stats = collect_stats() # collect statistics
enc = encrypt(cipher, stats) # encrypt before writing to file

# open file, write, then close
with open("/tmp/out.txt","wb") as file:
    file.write(enc)
